module.exports = {
    local: 'http://localhost:3000/',
    prod: 'https://gitlab-test-prod.herokuapp.com/',
    qa: 'https://gitlab-qa.herokuapp.com/'
}